/*

	diambil dari: https://www.callicoder.com/golang-basic-types-operators-type-conversion/

*/
package gohomework1

import (
	"fmt"
	"reflect"
	s "strings"
)

func char() {
	var myByte byte = 'a'
	var myRune rune = '♥'

	fmt.Printf("%c = %d and %c = %U\n", myByte, myByte, myRune, myRune)
}

func deferFunction() {
	defer fmt.Println("world")

	fmt.Println("hello")
}

func ifOne() {
	var a int = 10

	if a < 20 {
		fmt.Println("a < 20")
	}
}

func ifTwo() {
	var a int = 200

	if a == 10 {
		fmt.Printf("Nilai a = 10\n")
	} else if a == 20 {
		fmt.Printf("Nilai a = 20\n")
	} else if a == 30 {
		fmt.Printf("Nilai a = 30\n")
	} else {
		fmt.Printf("Semua nilai salah\n")
	}
	fmt.Printf("Nilai dari a adalah: %d\n", a)
}

func ifContinueBreak() {
	var a int = 10

	for a < 20 {
		if a == 12 {
			a += 1
			continue
		}
		a++
		if a > 15 {
			break
		}
		fmt.Printf("Nilai a: %d\n", a)
	}
}

func konstanta() {

	const mainCodingLang = "Go"
	const kiamatMakinDekat = true

	const angka1, angka2 = 25, 8

	const (
		nomorPegawai = "P001"
		gaji         = 50000000
	)

	const negaraKu string = "Indonesia"

	const gajiBersihSetelahSetorIstri = gaji - 49000000

	fmt.Println(mainCodingLang)
	fmt.Println(kiamatMakinDekat)
	fmt.Println(angka1)
	fmt.Println(angka2)
	fmt.Println(nomorPegawai)
	fmt.Println(gaji)
	fmt.Println(negaraKu)
	fmt.Println(gajiBersihSetelahSetorIstri)

	// ./konstanta.go:28:7: cannot assign to gaji
	//gaji = 10000000

}

func loopfor() {
	var i, j int

	for i = 1; i < 10; i++ {
		fmt.Println(i)
		for j = 1; j <= i; j++ {
			fmt.Println(j)
		}
	}
}

func defaultVariabel() {

	// unsigned-integer
	var defUint8 uint8
	var defUint16 uint16
	var defUint32 uint32
	var defUint64 uint64
	var defUint uint

	// signed-integer
	var defInt8 int8
	var defInt16 int16
	var defInt32 int32
	var defInt64 int64
	var defInt int

	// string
	var defString string

	// floating-point
	var defFloat32 float32
	var defFloat64 float64

	// complex
	var defComplex64 complex64
	var defComplex128 complex128

	// alias
	var defByte byte
	var defRune rune

	fmt.Println("\nNilai default untuk uint8 = ", defUint8)
	fmt.Println("Nilai default untuk uint16 = ", defUint16)
	fmt.Println("Nilai default untuk uint32 = ", defUint32)
	fmt.Println("Nilai default untuk uint64 = ", defUint64)
	fmt.Println("Nilai default untuk uint = ", defUint)

	fmt.Println("\nNilai default untuk int8 = ", defInt8)
	fmt.Println("Nilai default untuk int16 = ", defInt16)
	fmt.Println("Nilai default untuk int32 = ", defInt32)
	fmt.Println("Nilai default untuk int63 = ", defInt64)
	fmt.Println("Nilai default untuk int = ", defInt)

	fmt.Println("\nNilai default untuk string = ", defString)

	fmt.Println("\nNilai default untuk float32 = ", defFloat32)
	fmt.Println("Nilai default untuk float64 = ", defFloat64)

	fmt.Println("\nNilai default untuk complex64 = ", defComplex64)
	fmt.Println("Nilai default untuk complex128 = ", defComplex128)

	fmt.Println("\nNilai default untuk byte = ", defByte)
	fmt.Println("Nilai default untuk rune = ", defRune)

}

func pointerFunction() {
	i, j := 42, 2701

	// p berisi memory address dari i
	p := &i
	// tampilkan isi dari memory address p
	fmt.Println(*p)
	// isi dari memory address yang ditunjuk p diubah
	*p = 21
	// implikasinya pada variabel i:
	fmt.Println(i)

	// p berisi memory address dari j
	p = &j
	// isi dari memory address yang ditunjuk p, diubah
	// menjadi isi memoery address yang lama, dibagi 37
	*p = *p / 37
	// implikasinya pada variabel j
	fmt.Println(j) // see the new value of j

	var pa *int

	fmt.Printf("pointer pa dengan tipe %T dan nilai %v\n", pa, pa)

}

func switchFunction() {

	var nilaiAngka int = 20
	var nilaiHuruf string

	switch nilaiAngka {
	case 90:
		nilaiHuruf = "A"
	case 80:
		nilaiHuruf = "B"
	case 50, 60, 70:
		nilaiHuruf = "C"
	default:
		nilaiHuruf = "D"
	}

	switch {
	case nilaiHuruf == "A":
		fmt.Println("Apik tenan!")
	case nilaiHuruf == "B":
		fmt.Println("Lumayan lah")
	case nilaiHuruf == "C", nilaiHuruf == "D":
		fmt.Println("Lulus sih ... tapi ..")
	case nilaiHuruf == "E":
		fmt.Println("Nangis bombay")
	default:
		fmt.Println("Nilai gak jelas, seperti wajah dosennya!")
	}
	fmt.Printf("Nilai anda =  %s\n", nilaiHuruf)
}

func typeCast() {
	var (
		angka1     uint8   = 21
		angka2     uint8   = 17
		angkaFloat float64 = 7.1
	)

	// ./typecast.go:14:11: cannot use "abc" (type string) as type uint8 in assignment
	//angka1 = "abc"
	fmt.Println(angka1 + angka2)
	// ./typecast.go:15:21: invalid operation: angka1 + angkaFloat (mismatched types uint8 and float64)
	//fmt.Println(angka1 + angkaFloat)
	fmt.Println(float64(angka1) + angkaFloat)
}

func varBoolean() {
	var (
		hasilPerbandingan bool
		angka1            uint8 = 21
		angka2            uint8 = 17
	)
	hasilPerbandingan = angka1 < angka2
	fmt.Printf("angka1 = %d\n", angka1)
	fmt.Printf("angka2 = %d\n", angka2)
	fmt.Println(reflect.TypeOf(hasilPerbandingan))
	fmt.Println(hasilPerbandingan)
}

func varString() {
	// Definisi string
	var str1 string = "UGM"
	var str2 = "Yogyakarta"
	var str3 = "universitas\ngadjah mada"

	var str3backtick = `universitas\ngadjah mada`

	// Lihat https://golang.org/pkg/strings/
	fmt.Println(str1)
	fmt.Println(len(str1))
	fmt.Println(s.Contains(str1, "GM"))
	fmt.Println(s.Title(str3))
	fmt.Println(str1[0])
	fmt.Println(s.Join([]string{str1, str2}, " "))
	fmt.Println(s.Join([]string{str3, str2}, "\n"))
	fmt.Println(s.Join([]string{str3backtick, str2}, "\n"))
	fmt.Println(reflect.TypeOf(str1))
	fmt.Println(reflect.TypeOf(str2))
	fmt.Println()

}
